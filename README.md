# Passwords

An Emacs-lisp package that generates secure passwords.

Installation and use are explained in the _commentary_ section of the file.
